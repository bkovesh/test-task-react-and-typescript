import React, {useState} from 'react';
import s from './style.module.sass';
import * as Hooks from '../../hooks';
const cns = require('classnames');


export type TableProps = {
  data: any
}

export type TableHeaderProps = {
  data: string[]
}

export type TableItemProps = {
  index: number
  data: any[]
}


function TableHeader({ data }: TableHeaderProps) {
  return (
    <tr className={s.tableHeader}>
      { data && data.map((item: string,i) => {
        return (
          <th
          key={`td-${item}-${i}`}
          className={s.th}
          >
            {item}
          </th>
        )
      }) }
    </tr>
  )
}


function TableItem({ data, index }: TableItemProps) {
  return (
    <tr className={s.tr}>
      <td className={s.td1}>
        {index}
      </td>
      { data && data.map((item,i) => {
        return (
          <td
          key={`td-${item}-${i}`}
          className={index%2!==0?s.td:s.tdGrey}
          >
            {item}
          </td>
        )
      }) }
    </tr>
  )
}


function Table({ data, }: TableProps) {
  let [global, globalActions] = Hooks.useGlobal();
  const headers = data ? Object.keys(data[0]) : [];
  headers.unshift('№')

  return (
  <div className={cns(s.tableContainer)}>
    <div className={cns(s.header)}>
      {global.selectedMenuItem ? global.selectedMenuItem : 'Таблица'}
    </div>
    <div className={cns(s.header2)}></div>
    <table className={cns(s.table)}>
      <tbody>
        <TableHeader
        data={headers}
        />
        { data && data.map((item: any,i:number) => {
          const data = Object.values(item);
          return (
          <TableItem
          key={`tr-${item}-${i}`}
          index={i+1}
          data={data}
          />
          )
        }) }
      </tbody>
    </table>
  </div>
  );
}

export default Table;
