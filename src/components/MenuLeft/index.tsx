import React, {useEffect, useState} from 'react';
import s from './style.module.sass';
import {MenuType, MenuItemType} from "../../data/menu";
import * as Hooks from '../../hooks';
const cns = require('classnames');


export type MenuProps = {
  data: MenuType
}

export type MenuItemProps = {
  open: boolean
  selected: string
  data: MenuItemType
  onClick: () => void
  onSelect: (a: string) => void
}


function MenuItem({open, selected, data, onClick, onSelect} : MenuItemProps) {
  const {name} = data;

  return (
    <div className={s.itemContainer}>
      <div className={s.itemHeader} onClick={onClick}>
        {name}
      </div>
      { open && data && data.items && data.items.map((item,i) => {
        const {name} = item;
        const isSelected = selected === name;
        return (
          <div
          key={`td-${item}-${i}`}
          className={cns(s.item,isSelected && s.itemSelected)}
          onClick={() => onSelect(name)}
          >
            {name}
          </div>
        )
      }) }
    </div>
  )
}


function MenuLeft({ data, }: MenuProps) {
  let [global, globalActions] = Hooks.useGlobal();
  let {selectedCatalog, openMenuLeft, selectedMenuItem} = global;

  const handleSelect = (name: string) => {
    if (selectedCatalog && selectedCatalog.includes(name)) {
      selectedCatalog = selectedCatalog.filter((item: string) => item!==name)
    } else {
      if (!selectedCatalog) {
        selectedCatalog = [name];
      } else {
        selectedCatalog = [...selectedCatalog,...[name]]
      }
    }
    globalActions.setState({selectedCatalog})
  }

  const handleOpen = () => globalActions.setState({openMenuLeft:!openMenuLeft})

  const handleSelectItem = (name: string) => globalActions.setState({selectedMenuItem:name});

  return (
    <div
    className={cns(openMenuLeft?s.menuContainer:s.menuContainerClosed,{noselect:true})}
    >
      { openMenuLeft &&
      <div className={cns(s.menu)}>
        <div
        className={cns(s.header)}
        onClick={handleOpen}
        >
          Меню
        </div>
        { data && data.map((item,i) => {
          const {name} = item;
          return (
          <MenuItem
          key={`tr-${item}-${i}`}
          selected={selectedMenuItem}
          open={selectedCatalog && selectedCatalog.includes(name)}
          data={item}
          onClick={() => handleSelect(name)}
          onSelect={handleSelectItem}
          />
          )
        }) }
      </div>
      }
      <div
      className={cns(s.menuBtnOpen)}
      onClick={handleOpen}
      >
        {openMenuLeft ? '<' : '>'}
      </div>
    </div>
  );
}

export default MenuLeft;
