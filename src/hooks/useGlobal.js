import React from "react";
import useGlobalHook from "use-global-hook";
import initialState from "../store";

const actions = {
  setState: (store, newState) => {
    let stringState = JSON.stringify({...store.state,...newState})
    localStorage.setItem('store',stringState)
    store.setState({...store.state,...newState});
  },
};

const useGlobal = useGlobalHook(React, initialState, actions);
export default useGlobal;
