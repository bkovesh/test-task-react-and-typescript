
interface InitialStoreType {
  openMenuLeft: boolean,
  selectedCatalog: string[],
  selectedMenuItem: string,
}


const initialState: InitialStoreType = {
  openMenuLeft: true,
  selectedCatalog: [],
  selectedMenuItem: '',
}


const initialStateFromLocalStorage: InitialStoreType = JSON.parse(localStorage.getItem('store') as string)

const state: InitialStoreType = initialStateFromLocalStorage ? initialStateFromLocalStorage : initialState

export default state;