export type MenuItemType = {
  name: string
  items: {name:string,options:{}}[]
}

export type MenuType = MenuItemType[]


export const menu : MenuType = [
  {
    name: 'Аналитика',
    items: [
      {
        name: 'Журнал останова оборудования',
        options: {},
      },
      {
        name: 'Наработка компрессоров',
        options: {},
      },
      {
        name: 'Наработка газопроводов',
        options: {},
      },
      {
        name: 'Коэффициент эксплуатации',
        options: {},
      },
      {
        name: 'История',
        options: {},
      },
    ]
  },
  {
    name: 'Каталоги',
    items: [
      {
        name: 'Журнал останова оборудования 2',
        options: {},
      },
      {
        name: 'Наработка компрессоров 2',
        options: {},
      },
      {
        name: 'Наработка газопроводов 2',
        options: {},
      },
      {
        name: 'Коэффициент эксплуатации 2',
        options: {},
      },
      {
        name: 'История 2',
        options: {},
      },
    ]
  },
]

