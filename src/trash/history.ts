export type HistoryItemType = {
  'ДО': string;
  'Месторождение': string;
  'КС': string;
  'КУ': string;
  'Дата': string;
  'Значение': string;
}

export const history : HistoryItemType[] = [
  {
    'ДО': '1',
    'Месторождение': 'ascasc',
    'КС': 'ascasc',
    'КУ': 'ascasc',
    'Дата': 'ascasc',
    'Значение': 'ascas',
  },
  {
    'ДО': '1',
    'Месторождение': 'ascasc',
    'КС': 'ascasc',
    'КУ': 'ascasc',
    'Дата': 'ascasc',
    'Значение': 'ascas',
  },
]

