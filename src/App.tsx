import React from 'react';
import * as Components from './components';
import {tables} from './data/tables';
import {menu} from './data/menu';
import * as Hooks from "./hooks";



function App() {
  let [global, globalActions] = Hooks.useGlobal();
  const data = global && global.selectedMenuItem && tables[global.selectedMenuItem];
  return (
    <div className="App">
      <div className="AppContainer">
        <Components.MenuLeft
        data={menu}
        />
        <Components.Table
        data={data}
        />
      </div>
    </div>
  );
}

export default App;
